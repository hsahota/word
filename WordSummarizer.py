import os


class WordSummarizer:
    
    def countFrequencies(self, outDir):
        if outDir.endswith("/") == False:
            outDir = outDir + "/"
        filenames = os.listdir(outDir)
        for filename in filenames:
            if filename.endswith(".docx"):
                os.system('docx2txt.sh ' + outDir + filename)
            elif filename.endswith(".pdf"):
                os.system('pdf2txt.py -o ' + outDir +  filename + '.txt ' + outDir + filename)
        filenames = os.listdir(outDir)
        for filename in filenames:
            if filename.endswith(".txt"):
                os.system("strings " + outDir +  filename  + " >> " + outDir + "dump.txt")
                print "-----end of strings call ----- for " + filename
        os.system("touch "+ outDir + "dump.txt")		
        #print("cat " + outDir + "dump.txt | sed '/^$/d' | tr ' ' '\n' | sed '/^$/d' | sort | uniq -c | sort -gr > " + outDir + )
        os.system("cat " + outDir + "dump.txt | tr ' ' '\n' > " + outDir + "words.txt")
        os.system("cat " + outDir + "words.txt" + "| sort | uniq -c | sort -gr > " + outDir + "result.out")
        print "--------end of function--------------"
                
if __name__ == "__main__":
    WordSummarizer().countFrequencies("./data/")
    f = open("./data/result.out", "r")
    lines = f.readlines()
    for line in lines:
        print(line)
