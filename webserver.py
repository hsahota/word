import cherrypy
import json
import imp
import simplejson
import urllib2
import smtplib
import sys
import os

from jinja2 import Environment
from jinja2 import FileSystemLoader
from WordSummarizer import WordSummarizer


class Root():
    _cp_config = {'tools.staticdir.on' : True,
                  'tools.staticdir.root' : os.path.abspath(".")+"/",
                  'tools.staticdir.dir' : "./"
    }
    env = Environment(loader=FileSystemLoader(os.path.abspath(".") + '/templates'))
    
    @cherrypy.expose
    def index(self):
        return self.dumpHtml()
        
    @cherrypy.expose
    def countFrequencies(self):
        os.system("rm /root/Word/data/*.txt /root/Word/data/*.out")
        WordSummarizer().countFrequencies("/root/Word/data/")
        f = open("/root/Word/data/result.out", "r")
        lines = f.readlines()
        data = ""
        for line in lines:
            print(line)
            data = data + line + "</BR>"
        return self.dumpHtml(filename = "", summary = data)
        
    @cherrypy.expose
    def upload(self, myFile):
        if (len(myFile.filename) == 0):
            return self.dumpHtml("ERROR: NO FILE UPLOADED!")
        # Although this just counts the file length, it demonstrates
        # how to read large files in chunks instead of all at once.
        # CherryPy reads the uploaded file into a temporary file;
        # myFile.file.read reads from that.
        
        writeFilePath = os.path.join("/root/Word/data/", myFile.filename)
        if os.path.isfile(writeFilePath):
            os.remove(writeFilePath)
        f = open(writeFilePath, "w")
        print "Writing file", writeFilePath
        while True:
            data = myFile.file.read(8192)
            f.write(data)
            if not data:
                break
        f.close()                        
        return self.dumpHtml(myFile.filename)

    def dumpHtml(self, filename = "", summary = ""):
        out = """ <html>
        <body>
        <a href='/'>Home</a>
            <h3>Note!</h3>
            <b> 1. For MS Word files, the extension needs to be .docx.</b> </br>
            <b> 2. The filenames must not contain any spaces.</b>
            
            <h2>Step 1: Upload one file at a time (.pdf or .docx)</h2>
            <form action="upload" method="post" enctype="multipart/form-data">
            filename: <input type="file" name="myFile" /><br />
            <input type="submit" />
            </form>
            
            Uploaded the following file successfully: <b>[%s]</b><br />
                        
            <h3>Step 1a. files uploaded so far:</h3> <b>%s</b>
            </br>
            <h4><a href='delete'>delete all files to start fresh!</a> </h4>
            
            <h2>Step2. Generate Summary (After uploading all files)</h2>
            <b>Note: This step may take a few minutes.</b>
            <a href='countFrequencies'>generate summary!</a>
            <h1>Summary: </h1>
            </br> <b> %s </b>
        </body></html>
        """
        files = os.listdir("/root/Word/data/")
        pdfAndWordFiles = []
        for file in files:
            if file.endswith(".pdf") or file.endswith(".docx"):
                pdfAndWordFiles.append(file)
        x = reduce(lambda x,y: x + "</br> " + y, pdfAndWordFiles, "")
        
        return out % (filename, x, summary)

    @cherrypy.expose
    def delete(self):
        os.system("rm /root/Word/data/*")
        return self.dumpHtml()
        
if __name__ == '__main__':
    cherrypy.quickstart(Root(), '/', "config.txt")
